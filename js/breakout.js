        /* JS Exercise
         */

        //
        const LINE_NUMBER = 5;
        const BRICK_PER_LINE = 8;
        const BRICK_WIDTH = 48;
        const BRICK_HEIGHT = 15;
        const BRICK_MARGIN = 2;
        const RACKET_WIDTH = 80;
        const RACKET_HEIGHT = 10;
        const MOVE_STEP = 40;
        const BRICK_COLORS = ["#503A22", "#88502F", "#A17048", "#D9C38A", "#F7DDAC"];
        const BALL_SPEED = 2;

        //
        let gameWidth = 400;
        let gameHeight = 300;
        let racketX; //
        let racketY; //
        let ballX = 100; //
        let ballY = 250; //
        let dirBallX = 1; //
        let dirBallY = -1; //
        let brickArray; //
        let session; //

        function racketInit() {
            racketX = (gameWidth / 2) - (RACKET_WIDTH / 2);
            racketY = (gameHeight - RACKET_HEIGHT);
        }

        //
        function bricksInit() {
            //
            brickArray = new Array(LINE_NUMBER);

            for(let i = 0; i < LINE_NUMBER; i++) {
                //
                brickArray[i] = new Array(BRICK_PER_LINE);

                //
                for(let j = 0; j < BRICK_PER_LINE; j++) {
                    //
                    brickArray[i][j] = 1;
                }
            }
            
            return 1;
        }

        function getGame() {
            return document.getElementById('game');
        }

        function getElement(type, typeClass) {
            let element = document.getElementById(type);

            if(!element) {
                element = document.createElement('div');

                element.classList.add(typeClass);
                element.setAttribute('id', type);
                const game = getGame();
                game.appendChild(element);
            }
            return element;
        }

        //
        function ballDisplay(x, y) {
            ball = getElement('ball', 'ball');
            ball.style.left = `${x-10}px`;
            ball.style.top = `${y-10}px`;
            ball.style.height = '10px';
            ball.style.width = '10px';
        }

        function displayBrick(x, y, color) {
            let name = `${x}px${y}`;
            const brick = getElement(name, 'brick');

            brick.style.left = `${x}px`;
            brick.style.top = `${y}px`;
            brick.style.width = `${BRICK_WIDTH}px`;
            brick.style.height = `${BRICK_HEIGHT}px`;
            brick.style.backgroundColor = color;
        }
        //
        function bricksDisplay() {
            let hasWon = true;
            const game = getGame();
            
            for(let i = 0; i < brickArray.length; i++) {
                let color = BRICK_COLORS[i];
                
                //
                for(let j = 0; j < brickArray[i].length; j++) {
                    if(brickArray[i][j] === 1) {
                        displayBrick(j * (BRICK_WIDTH + BRICK_MARGIN),
                            i * (BRICK_HEIGHT + BRICK_MARGIN), color);
                        hasWon = false; //
                    } else {
                        displayBrick(j * (BRICK_WIDTH + BRICK_MARGIN),
                            i * (BRICK_HEIGHT + BRICK_MARGIN), '#ffffff');
                    }
                }
            }
            return hasWon;
        }

        function racketDisplay() {
            const racket = getElement('racket', 'racket');
            racket.style.left = `${racketX}px`;
            racket.style.top = `${racketY}px`;
            racket.style.width = `${RACKET_WIDTH}px`;
            racket.style.height = `${RACKET_HEIGHT}px`;
        }

        // 
        function computeDirections() {
            //
            if((ballX + dirBallX * BALL_SPEED) >  gameWidth) {
                dirBallX = -1;
            } else if((ballX + dirBallX * BALL_SPEED) <  0) {
                dirBallX = 1;
            }

            //
            if((ballY + dirBallY * BALL_SPEED) >  gameHeight) {
                gameOver(); //
                return false;
            } else {
                //
                if((ballY + dirBallY * BALL_SPEED) <  0) {
                    dirBallY = 1;
                }
                //
                else if(((ballY + dirBallY * BALL_SPEED) > (gameHeight - RACKET_HEIGHT)) &&
                    ((ballX + dirBallX * BALL_SPEED) >= racketX) && ((ballX + dirBallX * BALL_SPEED) <= (racketX + RACKET_WIDTH))) {
                    dirBallY = -1;
                    dirBallX = 2 * (ballX - (racketX + RACKET_WIDTH / 2)) / RACKET_WIDTH;
                }
            }
            return true;
        }

        //
        function brickCollisionTest() {
            if(ballY <= (BRICK_MARGIN + BRICK_HEIGHT) * LINE_NUMBER) {
                //
                let positionY = Math.floor(ballY / (BRICK_HEIGHT + BRICK_MARGIN));
                let positionX = Math.floor(ballX / (BRICK_WIDTH + BRICK_MARGIN));

                if(brickArray[positionY][positionX] === 1) {
                    brickArray[positionY][positionX] = 0;
                    dirBallY = 1;
                }
            }
        }

        function gameOver() {
            clearInterval(session);
            alert("Game over !");
        }

        function winner() {
            clearInterval(session);
            alert("You won !");
        }

        //
        function refreshGame() {
            //
            if(bricksDisplay()) {
                winner();
                return;
            }

            racketDisplay();

            //
            if(!computeDirections()) {
                return;
            }

            brickCollisionTest();

            //
            ballX += dirBallX * BALL_SPEED;
            ballY += dirBallY * BALL_SPEED;

            ballDisplay(ballX, ballY);
        }

        //
        function moveCheck(e) {
            //
            if(e.keyCode === 39) {
                if((racketX + MOVE_STEP + RACKET_WIDTH) <= gameWidth) {
                    racketX += MOVE_STEP;
                }
            }
            //
            else if(e.keyCode === 37) {
                if((racketX - MOVE_STEP) >= 0) {
                    racketX -= MOVE_STEP;   
                }
            }
        }

        //
        function gameInit() {
            //
            racketInit();
            bricksInit();

            //
            bricksDisplay();

            //
            session = setInterval(refreshGame, 10);

            //
            document.addEventListener('keydown', moveCheck);
        }

        gameInit();
