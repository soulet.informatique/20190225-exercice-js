Exercise Algorithm tools
========================


Follow next steps :

* Read js/breakout.js. Make a function tree. Input functions should be highlighted.
* Comment all lines with '//' in the file js/breakout.js
* Explain line by line the functions :
** bricksInit
** bricksDisplay
* Change bricks displaying :
<pre>
Line1:   XX   
Line2:  XXXX
Line3: XXXXXX
Line4:  XXXX
Line5:   XX
</pre>
* Implement a second ball
